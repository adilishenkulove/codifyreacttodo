
import { Main } from './Sections/Main';
import "./assets/sass/main.scss"

function App() {
  return (
    <div className="App">
      <Main name="Adil" />
    </div>
  );
}

export default App;
