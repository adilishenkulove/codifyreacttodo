import React, { useState } from "react";

export const TodoHeader = (props) => {
  const [state, setState] = useState({
    task: "",
    type: "danger-1",
  });

  return (
    <div className="header">
      <div className="header__input">
        <input
          onChange={(e) => {
            setState((old) => ({
              ...old,
              task: e.target.value,
            }));
          }}
          type="text"
        />
      </div>

      <select
        onChange={(e) => {
          setState((old) => ({
            ...old,
            type: e.target.value,
          }));
        }}
        className="header__select"
      >
        <option value="danger-1">Danger</option>
        <option value="pink-2">Pink</option>
        <option value="violet-3">Violet</option>
        <option value="primary-4">Primary</option>
        <option value="success-5">Success</option>
      </select>

      <div className="header__btn">
        <button
          onClick={() => {
            props.createTask(state.task, state.type);
          }}
        >
          Submit
        </button>
      </div>
      <div
        onClick={() => {
          props.isChecked ? props.viewNotChecked() : props.viewChecked();
        }}
        className={`${props.isChecked ? "red" : "blue"}`}
      >
        {props.isChecked ? "Back" : "View all checked"}
      </div>
    </div>
  );
};
