import React from "react";
import { TodoItem } from "./TodoItem";

export const TodoList = (props) => {
  return (
    <div>
      {props.list.map((item, index) => {
        console.log();
        return (
          <TodoItem
            key={item.id}
            deleteItem={props.deleteItem}
            checkedItem={props.checkedItem}
            id={item.id}
            checked={item.checked}
            task={item.task}
            type={item.type.replace(/-\d+/g, "")}
            showEdit={props.showEdit}
            item={item}
          />
        );
      })}
    </div>
  );
};
