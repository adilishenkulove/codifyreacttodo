import React from "react";

export const TodoSearch = (props) => {
  return (
    <div className="search">
      <div className="search__input">
        <input
          onChange={(e) => {
            props.search(e.target.value);
            // console.log(e.target.value);
          }}
          type="text"
          placeholder="Search"
        />
      </div>
    </div>
  );
};
