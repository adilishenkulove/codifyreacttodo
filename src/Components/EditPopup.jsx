import React, { useState } from "react";

export const EditPopup = ({ title, item, close, edit }) => {
  // const [state, setState] = useState()
  const [data, dataState] = useState(item);
  return (
    <div className="popup">
      <div className="popup__content">
        <div className="popup__title">{title}</div>
        <div className="popup__form">
          <div className="popup__input">
            <input
              defaultValue={data.task}
              onChange={(e) => {
                dataState((old) => ({
                  ...old,
                  task: e.target.value,
                }));
              }}
              type="text"
            />
          </div>
          <div className="popup__select">
            <select
              onChange={(e) => {
                dataState((old) => ({
                  ...old,
                  type: e.target.value,
                }));
              }}
              defaultValue={data.type}
              name=""
              id=""
            >
              <option value="danger-1">Danger</option>
              <option value="pink-2">Pink</option>
              <option value="violet-3">Violet</option>
              <option value="primary-4">Primary</option>
              <option value="success-5">Success</option>
            </select>
          </div>
          <div className="popup__btn">
            <button
              onClick={() => {
                // console.log(data);
                edit(data);
                close();
              }}
            >
              Save Changes
            </button>
          </div>
        </div>
      </div>
      <div
        onClick={() => {
          close();
        }}
        className="popup__bg"
      ></div>
    </div>
  );
};
