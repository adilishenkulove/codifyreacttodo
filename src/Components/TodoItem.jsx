import React from "react";

export const TodoItem = (props) => {
  return (
    <div className={`item ${props.type}`}>
      <div className="checkbox">
        <input
          onChange={(e) => {
            props.checkedItem(props.id, e.target.checked);
          }}
          defaultChecked={props.checked}
          id={props.id}
          type="checkbox"
          className="checkbox__input"
        />
        <label
          htmlFor={props.id}
          className={`checkbox__label item__title ${
            props.checked ? "checked" : ""
          }`}
        >
          {props.task}
        </label>
      </div>
      <div
        className="item__delete"
        onClick={() => {
          props.deleteItem(props.id);
        }}
      >
        X
      </div>
      <div
        onClick={() => {
          props.showEdit(props.item);
        }}
        className="item__edit"
      >
        Edit
      </div>
    </div>
  );
};
