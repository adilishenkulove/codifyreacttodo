import React, { useState, useEffect } from "react";

import { TodoList } from "../Components/TodoList";
import { TodoHeader } from "../Components/TodoHeader";
import { TodoSearch } from "../Components/TodoSearch";
import { EditPopup } from "../Components/EditPopup";

export const Main = (props) => {
  const [state, setState] = useState({
    list: [
      {
        id: Math.floor(Math.random() * 5000),
        task: "Adil",
        type: "success-5",
        checked: false,
      },
      {
        id: Math.floor(Math.random() * 5000),
        task: "Uluk",
        type: "danger-1",
        checked: false,
      },
    ],
    filterList: [],
    showSearch: false,
    isChecked: false,
    showPopup: false,
    editItem: null,
  });

  useEffect(() => {
    setState((old) => ({
      ...old,
      filterList: old.list,
    }));
  }, [state.list]);

  // useEffect(() => {
  //   let list = state.list.sort((a, b) => {
  //     return (
  //       Number(a.type.replace(/\D+/g, "")) - Number(b.type.replace(/\D+/g, ""))
  //     );
  //   });

  //   setState((old) => ({
  //     ...old,
  //     list,
  //     filterList: list,
  //   }));
  // }, [state.list]);

  const sortList = () => {
    return state.list.sort((a, b) => {
      return (
        Number(a.type.replace(/\D+/g, "")) - Number(b.type.replace(/\D+/g, ""))
      );
    });
  };

  useEffect(() => {
    let list = sortList();

    setState((old) => ({
      ...old,
      list,
      filterList: list,
    }));

    filterChecked();
  }, [state.list]);

  const createTask = (value, type) => {
    let list = state.list;
    const data = {
      id: Math.floor(Math.random() * 5000),
      task: value,
      type: type,
      checked: false,
    };
    list.push(data);
    setState((old) => ({
      ...old,
      list: sortList(),
    }));

    filterChecked();
  };

  const deleteItem = (id) => {
    let list = state.list.filter((item) => item.id !== id);
    setState((old) => ({
      ...old,
      list,
      filterList: list,
    }));
  };

  const checkedItem = (id, checked) => {
    let list = state.list.map((item) => {
      if (item.id === id) {
        item.checked = checked;
      }
      return item;
    });
    setState((old) => ({
      ...old,
      list,
    }));
  };

  const filterChecked = () => {
    let filterList = state.list.filter((item) => !item.checked);
    setState((old) => ({
      ...old,
      filterList,
    }));
  };

  const viewChecked = () => {
    let filterList = state.list.filter((item) => item.checked);
    setState((old) => ({
      ...old,
      filterList,
      isChecked: !old.isChecked,
    }));
  };

  const viewNotChecked = () => {
    let filterList = state.list.filter((item) => !item.checked);
    setState((old) => ({
      ...old,
      filterList,
      isChecked: !old.isChecked,
    }));
  };

  const search = (value) => {
    let filterList = state.list.filter((item) => {
      return item.task.toLowerCase().includes(value);
    });
    setState((old) => ({
      ...old,
      filterList,
    }));
  };

  const showEdit = (item) => {
    setState((old) => ({
      ...old,
      showPopup: true,
      editItem: item,
    }));
  };

  const closeEdit = () => {
    setState((old) => ({
      ...old,
      showPopup: false,
      editItem: null,
    }));
  };

  const editTask = (item) => {
    let list = state.list.map((elem) => {
      if (elem.id === item.id) {
        elem.task = item.task;
        elem.type = item.type;
      }
      return elem;
    });

    setState((old) => ({
      ...old,
      editItem: item,
      list
    }));
  };
  return (
    <div className="main">
      {state.showPopup && (
        <EditPopup
          close={closeEdit}
          edit={editTask}
          title="Edit"
          item={state.editItem}
        />
      )}
      <div className="main__top">
        <h1 className="title">To do</h1>
        <div
          onClick={() => {
            setState((old) => ({
              ...old,
              showSearch: !old.showSearch,
            }));
          }}
          className="search__btn"
        >
          Search items
        </div>
      </div>
      {state.showSearch && <TodoSearch search={search} />}
      <TodoHeader
        isChecked={state.isChecked}
        viewChecked={viewChecked}
        viewNotChecked={viewNotChecked}
        createTask={createTask}
      />
      <TodoList
        checkedItem={checkedItem}
        deleteItem={deleteItem}
        list={state.filterList}
        showEdit={showEdit}
      />
    </div>
  );
};
